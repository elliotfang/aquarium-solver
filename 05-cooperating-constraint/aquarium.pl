#!/usr/bin/env perl
use 5.024;
use warnings;
use autodie ':all';
use English '-no_match_vars';
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;

use Term::ANSIColor;
use Try::Catch;
use Storable 'dclone';

$|++;
my $puzzle = load_puzzle(shift);
solve_puzzle($puzzle);
print_puzzle($puzzle);

sub load_puzzle ($filename) {
   my $fh =
     $filename eq '-'    # filename '-' means 'read from standard input'
     ? \*STDIN
     : do { open my $fh, '<', $filename; $fh };

   # just get everything
   my @items        = split m{\D+}mxs, scalar readline $fh;
   my $n            = sqrt(@items + 1) - 1;
   my @items_by_col = splice @items, 0, $n;
   my @items_by_row = splice @items, 0, $n;
   my @field        = map { [splice @items, 0, $n] } 1 .. $n;

   return {
      n            => $n,
      items_by_col => \@items_by_col,
      items_by_row => \@items_by_row,
      field        => \@field,
   };
} ## end sub load_puzzle ($filename)

sub crossing ($A11, $A12, $A21, $A22) {
   return ' ' if ($A11 == $A12) && ($A11 == $A21) && ($A11 == $A22);
   return '-' if ($A11 == $A12) && ($A21 == $A22);
   return '|' if ($A11 == $A21) && ($A12 == $A22);
   return '+';
} ## end sub crossing

sub recolor ($string) {
   $string =~ s{(~+)}
{color('bold blue') . ('~' x length $1) . color('bold white')}egmsx;
   $string =~ s{(X+)}
{color('bold red') . ('X' x length $1) . color('bold white')}egmsx;
   return $string;
} ## end sub recolor ($string)

sub print_puzzle ($puzzle) {
   my $n          = $puzzle->{n};
   my @minus_one  = (-1) x $n;
   my @previous   = (-1, @minus_one, -1);
   my $horizontal = '#-------';
   my $vertical   = '|       ';
   my $i          = 0;
   print color('bold white');
   say '';
   my $cwidth = length $horizontal;
   my $by_column = join '', map {
      my $l     = length $_;
      my $delta = $cwidth - $l;
      $delta++ if $delta % 2;
      my $blanks = ' ' x ($delta / 2);
      substr $blanks . $_ . $blanks, 0, $cwidth, '';
   } '', $puzzle->{items_by_col}->@*;
   say $by_column;
   say '';
   for my $row ($puzzle->{field}->@*, [@minus_one]) {
      my @current = (-1, $row->@*, -1);
      my ($up_line, $mid1, $mid2) = ('') x 3;
      for my $j (1 .. $#current) {
         my $status =
           ($i < $n) && ($j <= $n) ? $puzzle->{status}[$i][$j - 1] : 0;
         my $pstatus =
              ($i > 0)
           && ($i < $n)
           && ($j <= $n) ? $puzzle->{status}[$i - 1][$j - 1] : 0;
         my $up = $horizontal;
         substr $up, 0, 1,
           crossing(@previous[$j - 1, $j], @current[$j - 1, $j]);
         $up =~ s{-}{ }gmxs if $current[$j] == $previous[$j];
         $up =~ s{\ }{~}gmxs if $status > 0 && $pstatus > 0;
         $up_line .= $up;

         my $left = $vertical;
         $left =~ s{\|}{ }gmxs if $current[$j] == $current[$j - 1];
         $left =~ s{\ }{~}gmxs if $status > 0;
         $mid1 .= $left;
         substr $left, 4, 1, 'X' if $status < 0;
         $mid2 .= $left;
      } ## end for my $j (1 .. $#current)
      $_ = recolor($_) for ($up_line, $mid1, $mid2);
      s{\s+\z}{}mxs for ($up_line, $mid1, $mid2);

      (my $left_blanks = $vertical) =~ s{\S}{ }gmxs;
      say $left_blanks, $up_line;
      $mid1 = $left_blanks . $mid1;
      my $n = length($left_blanks) - 2;
      $left_blanks = sprintf "%${n}s  ", $puzzle->{items_by_row}[$i]
        if $mid2 =~ m{\S}mxs;
      $mid2 = $left_blanks . $mid2;
      do { say for $mid1, $mid2, $mid1 } if $mid2 =~ m{\S}mxs;
      @previous = @current;
      ++$i;
   } ## end for my $row ($puzzle->{...})
   say '';
   print color('reset');
} ## end sub print_puzzle ($puzzle)

sub adjust_water_level ($puzzle) {
   my ($n, $field, $status) = $puzzle->@{qw< n field status >};
   my $n_changes = 0;
   for my $i (0 .. $n - 1) {    # iterate rows from top to bottom
      my %expected;

      # first sweep: adjust vertical flooding, set expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         # vertical condition from second row on...
         if (($i > 0) && ($id == $field->[$i - 1][$j])) {
            if ($st < $status->[$i - 1][$j]) { # possible mismatch?
               if ($st == 0) { # current cell is *unknown*, relax!
                  $st = $status->[$i][$j] = 1;  # fill with water
                  $n_changes++;
               }
               elsif ($status->[$i - 1][$j] == 0) { # previous is unknown
                  # let's just ignore this for the moment...
               }
               else {
                  die "wrong vertical leveling for aquarium $id\n";
               }
            }
         }

         $expected{$id} ||= $st; # change only if unknown
      }
   
      # second sweep: adjust horizontal flooding based on expectations
      for my $j (0 .. $n - 1) {
         my $id = $field->[$i][$j];
         my $st = $status->[$i][$j];

         if ($st == 0) {
            if ($expected{$id}) {
               $st = $status->[$i][$j] = $expected{$id};
               $n_changes++;
            }
         }
         elsif ($st != $expected{$id}) {
            die "wrong horizontal leveling for aquarium $id\n"
         }
      } ## end for my $j (0 .. $n - 1)
   } ## end for my $i (0 .. $n - 1)
   return $n_changes;
} ## end sub assert_water_level ($puzzle)

sub assert_boundary_conditions ($puzzle) {
   my ($n, $status, $items_by_row, $items_by_col) = 
      $puzzle->@{qw< n status items_by_row items_by_col >};

   # the field is square and this is an advantage, $i and $j can be
   # thought as either row-column or column-row
   for my $i (0 .. $n - 1) {
      my $i1 = $i + 1; # useful for the exception
      my ($water_row, $empty_row, $water_col, $empty_col) = (0) x 4;
      for my $j (0 .. $n - 1) {
         $water_row++ if $status->[$i][$j] > 0;
         $empty_row++ if $status->[$i][$j] < 0;
         $water_col++ if $status->[$j][$i] > 0;
         $empty_col++ if $status->[$j][$i] < 0;
      }

      die "too many filled cells in row $i1\n"
         if $water_row > $items_by_row->[$i];

      die "too many empty cells in row $i1\n"
         if $empty_row > $n - $items_by_row->[$i];

      die "too many filled cells in col $i1\n"
         if $water_col > $items_by_col->[$i];

      die "too many empty cells in col $i1\n"
         if $empty_col > $n - $items_by_col->[$i];

   }
   return $puzzle;
}

sub apply_constraints ($puzzle) {
   my $changes = -1;
   while ($changes != 0) {
      $changes = 0;
      $changes += adjust_water_level($puzzle);
      assert_boundary_conditions($puzzle);
   }
}

sub moves_iterator ($puzzle) {
   my ($n, $field) = $puzzle->@{qw< n field >};
   my $original_status = dclone($puzzle->{status});
   my $i = $n - 1;
   my $j = 0;
   my %done;
   return sub {
      my $status = dclone($original_status);
      while ($i >= 0) {
         while ($j < $n) {
            next if $status->[$i][$j]; # look for unknown spots
            my $id = $field->[$i][$j];
            next if $done{$id}++;
            for my $tmp_j ($j .. $n - 1) {
               next unless $field->[$i][$tmp_j] == $id;
               $status->[$i][$tmp_j] = 1; # try water here
            }
            return $status;
         }
         continue {
            $j++;
         }
      }
      continue {
         $i--;
         $j = 0;
      }
      return;
   };
}

sub is_complete ($puzzle) {
   my ($n, $items_by_row, $status) = $puzzle->@{qw< n items_by_row status >};
   my $missing = 0;
   for my $i (0 .. $n - 1) {
      $missing += $items_by_row->[$i];
      for my $j (0 .. $n - 1) {
         $missing-- if $status->[$i][$j] > 0;
      }
   }
   return $missing == 0;
}

sub solve_puzzle ($puzzle) {
   my $n = $puzzle->{n};
   $puzzle->{status} = [ map { [(0) x $n] } 1 .. $n ];
   my @stack;
   my $done;
   while (! $done) {
      try {
         apply_constraints($puzzle);

         # if there are still unknown items, let's take a guess
         $done = is_complete($puzzle);
         if (! $done) {
            my $guesser = moves_iterator($puzzle);
            my $status = $guesser->(scalar @stack) # do first guess!
               or die "no more guesses here\n";

            # save guesser for backtracking
            push @stack, $guesser;

            # of course this is the new status
            $puzzle->{status} = $status;
         }
      }
      catch {
         (my $e = $_) =~ s{\s+\z}{}gmxs;
         while (@stack) { # backtrack until there's a new guess
            if (my $status = $stack[-1]->(scalar @stack - 1)) {
               $puzzle->{status} = $status;
               last;
            }
            pop @stack;
         }
         die "unfeasible <$e>\n" unless @stack;
      };
   }
   return $puzzle;
}
