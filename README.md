# Aquarium Solver

This repository contains an example solver for the [Aquarium][] online
puzzle. It is paired to a series of posts in blog [ETOOBUSY][], tagged
with [aquarium puzzle game][].

Different *stages* of the implementation are contained in the several
sub-directories, ordered by their increasing inclusion of more code.

If you want to run the code in one stage, you can take advantage of the
provided script `run.sh`, like this:

```shell
$ ./run.sh 01-parse
{"field":[["1","1","2","3","3","3"],["1","2","2","2","3","4"],["1","2","3","3","3","4"],["2","2","2","5","5","4"],["6","2","5","5","5","4"],["6","6","6","4","4","4"]],"items_by_row":["4","5","5","3","1","3"],"items_by_col":["4","5","5","3","3","1"],"n":6}
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[Aquarium]: https://www.puzzle-aquarium.com/
[ETOOBUSY]: https://github.polettix.it/ETOOBUSY/
[aquarium puzzle game]: https://github.polettix.it/ETOOBUSY/tagged/#aquarium%20puzzle%20game
