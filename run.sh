#!/bin/sh
md="$(dirname "$(readlink -f "$0")")"
stage="${1:-"01-parse"}"
puzzle="${2:-"$md/example.aqp"}"
perl -I "$md/local/lib/perl5" "$md/$stage/aquarium.pl" "$puzzle"
